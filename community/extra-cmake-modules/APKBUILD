# Contributor: k0r10n <k0r10n.dev@gmail.com>
# Contributor: Ivan Tham <pickfire@riseup.net>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=extra-cmake-modules
pkgver=5.109.0
pkgrel=0
_commit="2dee113bd3ab747245209ed1431427691dfa246d"
pkgdesc="Extra CMake modules"
url="https://invent.kde.org/frameworks/extra-cmake-modules"
arch="noarch"
license="BSD-3-Clause"
depends="cmake"
makedepends="
	py3-sphinx
	samurai
	"
checkdepends="
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qttools-dev
	"
_repo_url="https://invent.kde.org/frameworks/extra-cmake-modules.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/extra-cmake-modules-$pkgver.tar.xz"
subpackages="$pkgname-doc"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/extra-cmake-modules.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DSphinx_BUILD_EXECUTABLE=/usr/bin/sphinx-build \
		-DBUILD_QTHELP_DOCS=ON
	cmake --build build
}

check() {
	# KDEFetchTranslations expects KDE's git setup
	ctest --test-dir build --output-on-failure -E "KDEFetchTranslations"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
62ae279433a89978c703d9f79b5070d57ca071b7c50461b8b1cd6ad9283d9c8130bf97050490897fe638efe9b5c8d16bd11f12bf7736cfccc9a594061b3bbdc6  extra-cmake-modules-5.109.0.tar.xz
"
