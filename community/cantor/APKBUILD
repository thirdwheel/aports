# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=cantor
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://edu.kde.org/cantor/"
pkgdesc="KDE Frontend to Mathematical Software "
license="GPL-2.0-or-later"
makedepends="
	analitza-dev
	extra-cmake-modules
	karchive-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	kparts-dev
	kpty-dev
	ktexteditor-dev
	ktextwidgets-dev
	kxmlgui-dev
	poppler-qt5-dev
	python3-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtwebengine-dev
	qt5-qtxmlpatterns-dev
	samurai
	syntax-highlighting-dev
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/education/cantor.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/cantor-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
579e1c79f50540b212fe6a26beac7af9367b9d87a9d364e37828e8a72aa6c0915c1a5da10769df0fa3fd726e3f7c78bb0b05a29dfd0bbbf520f73abb78793f2f  cantor-23.04.3.tar.xz
"
