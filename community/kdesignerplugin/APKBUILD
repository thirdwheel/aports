# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kdesignerplugin
pkgver=5.109.0
pkgrel=0
pkgdesc="Integration of Frameworks widgets in Qt Designer/Creator"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends_dev="
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kiconthemes-dev
	kio-dev
	kitemviews-dev
	kplotting-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	sonnet-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/kdesignerplugin.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kdesignerplugin-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kdesignerplugin.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
293c3527894c9958fb96a51a712f3f069291e594146811b3ddcd83fc556f1d2026a9c29a6df8405df9cfb145838ad5777073182e3fbe20e20bfa2b629ae84f69  kdesignerplugin-5.109.0.tar.xz
"
