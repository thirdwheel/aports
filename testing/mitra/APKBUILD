# Contributor: Celeste <cielesti@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=mitra
pkgver=1.32.0
pkgrel=0
pkgdesc="ActivityPub microblogging platform written in Rust"
url="https://mitra.social/@mitra"
# riscv64: vite webapp fails to build: 'Parse error @:1:38'
arch="all !riscv64"
license="AGPL-3.0-only"
depends="postgresql"
makedepends="
	cargo
	cargo-auditable
	nodejs
	npm
	openssl-dev
	"
install="$pkgname.pre-install $pkgname.post-install"
pkgusers="mitra"
pkggroups="mitra"
subpackages="$pkgname-doc $pkgname-openrc"
source="mitra-$pkgver.tar.gz::https://codeberg.org/silverpill/mitra/archive/v$pkgver.tar.gz
	mitra-web-$pkgver.tar.gz::https://codeberg.org/silverpill/mitra-web/archive/v$pkgver.tar.gz
	mitra.initd
	init.sql
	config.yaml
	"
builddir="$srcdir/mitra"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked

	cd "$srcdir/mitra-web"

	npm ci --foreground-scripts
}

build() {
	cargo auditable build --frozen --release \
		--features production

	cd "$srcdir/mitra-web"

	echo 'VITE_BACKEND_URL=' > .env.local
	npm run build
}

check() {
	# These tests require a database connection
	cargo test --frozen --workspace \
		--exclude mitra-models -- \
		--skip test_follow_unfollow \
		--skip test_hide_reblogs \
		--skip test_subscribe_unsubscribe \
		--skip test_get_jrd

	cd "$srcdir/mitra-web"

	npm run test
}

package() {
	install -Dm755 target/release/mitra -t "$pkgdir"/usr/bin
	install -Dm755 target/release/mitractl -t "$pkgdir"/usr/bin

	mkdir -p "$pkgdir"/usr/share/webapps
	cp -r "$srcdir"/mitra-web/dist \
		"$pkgdir"/usr/share/webapps/mitra

	install -Dm644 docs/* -t "$pkgdir"/usr/share/doc/$pkgname
	install -Dm644 config.yaml.example \
		contrib/Caddyfile contrib/*.nginx \
		contrib/monero/wallet.conf \
		-t "$pkgdir"/usr/share/doc/$pkgname/examples

	install -Dm640 -g mitra "$srcdir"/config.yaml -t "$pkgdir"/etc/mitra
	install -dm755 -o mitra -g mitra "$pkgdir"/var/lib/mitra
	install -Dm644 "$srcdir"/init.sql -t "$pkgdir"/var/lib/mitra
	install -Dm755 "$srcdir"/mitra.initd "$pkgdir"/etc/init.d/mitra
}

sha512sums="
443bfb31761494c513d5fc286660582944e20825bd8aa079b5f5177c5e4feccf94c1d38e43fe9e6d6f55c2204df7bfe3d1469dbbb77d0ad75072e3ece869b28b  mitra-1.32.0.tar.gz
aa71972c35d468099327b1b9b80f2fded0008236ce3e5b5d7e36da9c71943864aefc4f623204954c076c1b447c175795bbaaf46adcd61b6bab33502e4a0afd39  mitra-web-1.32.0.tar.gz
691f84f5dfdddc176e75792ab03ff167054246e75ced51be47a89f405ae55ebe5eb6280b73c1b467b5ecbe8539f6108fb3d86873d50fcc4f4b8c5b182632acb0  mitra.initd
02a40bf9dee625bf4b4783aa3588428313efa1e4d5ec5b8e64d76b9c6248f4d72b9d1287fba72b9d5d62c34352131d5d1609453583e257c5e8a1d2a787779147  init.sql
e131d3eb3cd5607e5c1962c8ec432f616cad845b5e45563a0181bf146db039598a13b5ac8705552203f5e8029f0326d954d198cecfeb1904feb5259f3df04750  config.yaml
"
