# Contributor: Celeste <cielesti@protonmail.com>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=py3-dj-database-url
pkgver=2.0.0
pkgrel=0
pkgdesc="Use Database URLs in your Django Application"
url="https://pypi.org/project/dj-database-url/"
arch="noarch"
license="BSD-3-Clause"
depends="py3-django"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/jazzband/dj-database-url/archive/v$pkgver/py3-dj-database-url-$pkgver.tar.gz
	typing-ext.patch
	"
builddir="$srcdir/dj-database-url-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
d2600997023b1e58c1e6285268a400aed57fbdff972a36025f47bcc2e57d3179f266379d7042dc75c6048b35f7aef8547828c88a2fad1e80f087e6f5680668b0  py3-dj-database-url-2.0.0.tar.gz
cac6ae60288dbd8d9f97e277d37acdab42483690a95897d2520588e260020a390b235e3d72dccfeab1ea903b2d60b9fd1c53b8c008724dbda59c031e0dc2359f  typing-ext.patch
"
