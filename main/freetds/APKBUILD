# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Michael Mason <ms13sp@gmail.com>
pkgname=freetds
pkgver=1.3.19
pkgrel=0
pkgdesc="Tabular Datastream Library"
url="https://www.freetds.org/"
arch="all"
license="GPL-2.0-or-later OR LGPL-2.0-or-later"
makedepends="openssl-dev>3 linux-headers readline-dev unixodbc-dev"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"
source="https://www.freetds.org/files/stable/freetds-$pkgver.tar.bz2"
options="!check"  # tests require running SQL server http://www.freetds.org/userguide/confirminstall.htm#TESTS

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-msdblib \
		--with-openssl=/usr \
		--enable-odbc \
		--with-unixodbc=/usr
	make
}

check() {
	cd "$builddir"/src/replacements
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="
4677d0eebac25c772c0b9daec7dca840896e710541ec3c810369b6717560dbdb68c5e94091487f90694fa9757f6873e3d5f74a113aeb953d076131652e7f0d2d  freetds-1.3.19.tar.bz2
"
