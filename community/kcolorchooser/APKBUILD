# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kcolorchooser
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/graphics/org.kde.kcolorchooser"
pkgdesc="A color palette tool, used to mix colors and create custom color palettes"
license="MIT"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/graphics/kcolorchooser.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcolorchooser-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7137758baa9bc16e9e6974dd69b37dae27fe9e9aa4e9565ef3d11e84ac84b67ba3286295a7262f3536e6ffdb6e81b2c47d2a0eaab7f132a7f1e82b0dd1fb3345  kcolorchooser-23.04.3.tar.xz
"
