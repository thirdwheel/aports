# Contributor: Hygna <hygna@proton.me>
# Contributor: Fabricio Silva <hi@fabricio.dev>
# Maintainer: Fabricio Silva <hi@fabricio.dev>
pkgname=pnpm
pkgver=8.6.11
pkgrel=0
pkgdesc="Fast, disk space efficient package manager"
url="https://pnpm.io"
arch="noarch"
license="MIT"
depends="nodejs"
source="https://registry.npmjs.org/pnpm/-/pnpm-$pkgver.tgz"
options="!check" # not implemented
builddir="$srcdir/package"

prepare() {
	default_prepare

	# remove node-gyp
	rm -rf dist/node-gyp-bin dist/node_modules/node-gyp
	# remove windows files
	rm -rf dist/vendor/*.exe

	# remove other unnecessary files
	find . -type f \( \
		-name '.*' -o \
		-name '*.cmd' -o \
		-name '*.bat' -o \
		-name '*.map' -o \
		-name '*.md' -o \
		-iname 'LICENSE*' -o \
		-iname 'README*' \) -delete
}

package() {
	local DESTDIR="$pkgdir"/usr/share/node_modules/pnpm

	mkdir -p "$DESTDIR"
	cp -R "$builddir"/* "$DESTDIR"/

	mkdir -p "$pkgdir"/usr/bin
	ln -sf ../share/node_modules/pnpm/bin/pnpm.cjs "$pkgdir"/usr/bin/pnpm
	ln -sf ../share/node_modules/pnpm/bin/pnpx.cjs "$pkgdir"/usr/bin/pnpx
}

sha512sums="
8ea927a69ba3e39b43cc9b0b70baa4031cad047657231f494d890636ad3fee9491820a62a3dc11c530e3e0d0368a53873e52790558c1e488f9771eb9c28322e4  pnpm-8.6.11.tgz
"
