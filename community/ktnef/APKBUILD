# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ktnef
pkgver=23.04.3
pkgrel=1
pkgdesc="API for handling TNEF data"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="
	kcalendarcore-dev
	kcalutils-dev
	kcontacts-dev
	ki18n-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
_repo_url="https://invent.kde.org/pim/ktnef.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/ktnef-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
e49daa0d6d341b074f9c2854f63656ccc165c045517ec3c091665453967761f78b160f92a79f930f8d98e6f676b33e717ed68dd3b725caf114d66f609f20662b  ktnef-23.04.3.tar.xz
"
