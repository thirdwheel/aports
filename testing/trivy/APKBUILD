# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=trivy
pkgver=0.44.1
pkgrel=0
pkgdesc="Simple and comprehensive vulnerability scanner for containers"
url="https://github.com/aquasecurity/trivy"
arch="all"
# s390x: tests SIGSEGV: https://github.com/aquasecurity/trivy/issues/430
arch="$arch !s390x"
license="Apache-2.0"
makedepends="btrfs-progs-dev go linux-headers lvm2-dev"
source="https://github.com/aquasecurity/trivy/archive/v$pkgver/trivy-$pkgver.tar.gz"
options="net !check" # needs tinygo to turn go into wasm for tests

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	export CGO_ENABLED=0

	go build -o trivy -ldflags "-X main.version=$pkgver" cmd/trivy/main.go
}

check() {
	go test ./...
}

package() {
	install -Dm755 trivy -t "$pkgdir"/usr/bin/
}

sha512sums="
a705da35f9fb047d21aaac1ab8c3f680d17b18caf491d14bd3db89960f2e239d096c63ac7d01049f9cd540f2f67c79e8ad7cf4846de08a842471304adc61086c  trivy-0.44.1.tar.gz
"
