# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kldap
pkgver=23.04.3
pkgrel=1
pkgdesc="LDAP access API for KDE"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="
	kcompletion-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	openldap-dev
	qtkeychain-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	cyrus-sasl-dev
	samurai
	"
_repo_url="https://invent.kde.org/pim/kldap.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kldap-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
89e8c62990e950243f9f5b69f9f49ff34a3b7e2958b675818924181e58a1ec11b10e73fe8d4c92095931981a0062573a5cafd4d65f0d92a8d5cc7f6e408a509f  kldap-23.04.3.tar.xz
"
