# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kactivities
pkgver=5.109.0
pkgrel=0
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
pkgdesc="Core components for the KDE's Activities"
url="https://community.kde.org/Frameworks"
license="GPL-2.0-or-later AND LGPL-2.1-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="qt5-qtbase-sqlite"
depends_dev="
	kconfig-dev
	kcoreaddons-dev
	kwindowsystem-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	boost-dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/kactivities.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kactivities-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kactivities.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ce8ec32853f0db9cb5e64356c39be88570fb4982d36cb097e6a2ff23fccf0f266da168186148680f5e7290c142968a8728185abb77023d69527a55bb7e4eb605  kactivities-5.109.0.tar.xz
"
