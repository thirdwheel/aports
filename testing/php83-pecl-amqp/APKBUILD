# Contributor: Fabio Ribeiro <fabiorphp@gmail.com>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php83-pecl-amqp
_extname=amqp
pkgver=2.0.0_rc1
_pkgver=${pkgver/_rc/RC}
pkgrel=0
pkgdesc="PHP 8.3 extension to communicate with any AMQP spec 0-9-1 compatible server - PECL"
url="https://pecl.php.net/package/amqp"
arch="all"
license="PHP-3.01"
_phpv=83
_php=php$_phpv
depends="$_php-common"
makedepends="$_php-dev rabbitmq-c-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$_pkgver.tgz"
builddir="$srcdir/$_extname-$_pkgver"

build() {
	phpize$_phpv
	./configure --prefix=/usr --with-php-config=php-config$_phpv
	make
}

check() {
	# Tests require running AMQP server, so basic check
	$_php -d extension="$builddir"/modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/$_php/conf.d
	mkdir -p $_confdir
	echo "extension=$_extname" > $_confdir/40_$_extname.ini
}

sha512sums="
dd2482d784c128dc5d2db7ccae650b4a49a540dddb851e60ccf07f1ad4656d319c5e897cd04b76acfd415fa8435ebf88eed0e9c0ecd9992c265de776d9eaf185  php-pecl-amqp-2.0.0_rc1.tgz
"
