# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kconfigwidgets
pkgver=5.109.0
pkgrel=0
pkgdesc="Widgets for KConfig"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later"
depends_dev="
	kauth-dev
	kcodecs-dev
	kconfig-dev
	kcoreaddons-dev
	kguiaddons-dev
	ki18n-dev
	kwidgetsaddons-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	kdoctools-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/kconfigwidgets.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kconfigwidgets-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kconfigwidgets.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure -E "kstandardactiontest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e819a615c5c3d86f4f9c343fd2f02587980705bd5f20ac9cef30e817ec16ab03da3a6d136bcfaae6a645cf02e9d416808ca831ef64a66d814199ac7dff4a8d90  kconfigwidgets-5.109.0.tar.xz
"
