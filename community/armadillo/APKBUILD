# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Maintainer: Holger Jaekel <holger.jaekel@gmx.de>
pkgname=armadillo
pkgver=12.6.1
pkgrel=0
pkgdesc="C++ library for linear algebra & scientific computing"
url="https://arma.sourceforge.net/"
arch="all"
license="Apache-2.0"
options="!check"  # Armadillo must be installed before the tests can be compiled
depends_dev="
	hdf5-dev
	superlu-dev
	"
makedepends="
	$depends_dev
	arpack-dev
	cmake
	openblas-dev
	samurai
	"
subpackages="$pkgname-dev"
source="https://downloads.sourceforge.net/project/arma/armadillo-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	mkdir -p "$pkgdir/usr/lib/cmake"
	mv "$pkgdir/usr/share/Armadillo/CMake"/* "$pkgdir/usr/lib/cmake"
	rm -rf "$pkgdir/usr/share"
}

sha512sums="
9b426aa32d152cfb117867de6f9fdff525aae1e525a05c0f2a941c2a123b916dd50ab060d8facaa1d317303d80e97f32bf5814c68912158d746ce4f09e77a5ea  armadillo-12.6.1.tar.xz
"
