# Contributor: Oleg Titov <oleg.titov@gmail.com>
# Maintainer: Oleg Titov <oleg.titov@gmail.com>
pkgname=py3-confection
pkgver=0.1.1
pkgrel=0
pkgdesc="Confection: the sweetest config system for Python"
url="https://github.com/explosion/confection"
arch="noarch"
license="MIT"
depends="
	python3
	py3-pydantic
	py3-srsly
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-doc $pkgname-pyc"
checkdepends="
	py3-catalogue
	py3-pytest-runner
	py3-pytest-xdist
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/explosion/confection/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/confection-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto
}

package() {
	python -m installer -d "$pkgdir" .dist/*.whl

	install -vDm644 README.md -t "$pkgdir/usr/share/doc/$pkgname/"

	rm -r "$pkgdir"/usr/lib/python3*/site-packages/confection/tests
}

sha512sums="
84bc35cfbefc6a08f30f6ec14b66e5ba8f0b10c19563c786f02a5465efec1cdf90cdd7a39e4143bac8edf049a226de63e11cd9d1488de409a97f334af3efbab1  py3-confection-0.1.1.tar.gz
"
