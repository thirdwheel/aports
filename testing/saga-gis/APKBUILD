# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Maintainer: Holger Jaekel <holger.jaekel@gmx.de>
pkgname=saga-gis
pkgver=9.1.1
pkgrel=0
pkgdesc="System for Automated Geoscientific Analyses"
url="https://saga-gis.sourceforge.io/en/index.html"
arch="all"
license="GPL-2.0-or-later"
depends="$pkgname-common"
_makedepends_mandatory="
	cmake
	gdal-dev
	proj-dev
	samurai
	tiff-dev
	unixodbc-dev
	wxwidgets-dev
	"
_makedepends_optional="
	curl-dev
	libharu-dev
	libpq-dev
	pdal-dev
	python3-dev
	qhull-dev
	swig
	vigra-dev
	"
# opencv not on s390x
# openmp not on armhf and s390x
case "$CARCH" in
	s390x)
		;;
	armhf)
		_makedepends_optional="$_makedepends_optional opencv-dev"
		;;
	*)
		_makedepends_optional="$_makedepends_optional opencv-dev openmp-dev"
		;;
esac
makedepends="$_makedepends_mandatory $_makedepends_optional"
subpackages="$pkgname-dev $pkgname-doc py3-$pkgname:py3 $pkgname-api $pkgname-gdi $pkgname-common::noarch"
source="https://downloads.sourceforge.net/project/saga-gis/SAGA%20-%209/SAGA%20-%20$pkgver/saga-$pkgver.tar.gz"
builddir="$srcdir/saga-$pkgver/$pkgname"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		local crossopts="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=ON \
		-DCMAKE_BUILD_TYPE=None \
		-DWITH_MRMR=ON \
		$crossopts
	cmake --build build
}

check() {
	./build/src/saga_core/saga_cmd/saga_cmd -h | grep -q $pkgver
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

api() {
	pkgdesc="$pkgdesc (shared library files)"
	amove "usr/lib/libsaga_api.so.*"
}

gdi() {
	pkgdesc="$pkgdesc (shared library files for developing graphical modules)"
	amove "usr/lib/libsaga_gdi.so.*"
}

common() {
	pkgdesc="$pkgdesc (architecture independent files)"
	amove "usr/share/saga"
}

py3() {
	pkgdesc="$pkgdesc (Python3 bindings)"
	amove usr/lib/python3*
}

sha512sums="
8890de7416921d6a3e27c1d0cfefc25f494192ca8b96b99f963f354505ec53b60ed2deef1878287f66c76d9f76d539e6f2ab04a03e0c6a9a7ade56673d50edff  saga-9.1.1.tar.gz
"
