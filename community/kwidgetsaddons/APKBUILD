# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kwidgetsaddons
pkgver=5.109.0
pkgrel=0
pkgdesc="Addons to QtWidgets"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="GPL-2.0-only AND LGPL-2.1-only AND Unicode-DFS-2016"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="
	mesa-dri-gallium
	xvfb-run
	"
_repo_url="https://invent.kde.org/frameworks/kwidgetsaddons.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwidgetsaddons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kwidgetsaddons.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# ktwofingertaptest, ktwofingerswipetest and and ksqueezedtextlabelautotest are broken
	xvfb-run ctest --test-dir build --output-on-failure -E '(ktooltipwidget|ktwofingertap|ktwofingerswipe|ksqueezedtextlabelauto)test'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0570281b93b56f6f0a6774511226526bb04f131b3d801b3533519d4eb80dc2e7993858a8e17761817136444bdbd9ef748e560a225c50c6230fcc7412d649a735  kwidgetsaddons-5.109.0.tar.xz
"
