# Contributor: Philipp Glaum <p@pglaum.de>
# Maintainer: Dhruvin Gandhi <contact@dhruvin.dev>
pkgname=todoman
_pyname=todoman
pkgver=4.3.1
pkgrel=2
pkgdesc="A simple CalDav-based todo manager"
url="https://pypi.org/project/todoman/"
arch="noarch"
license="ISC"
depends="
	python3
	py3-atomicwrites
	py3-click
	py3-click-log
	py3-dateutil
	py3-humanize
	py3-icalendar
	py3-parsedatetime
	py3-urwid
	py3-xdg
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-setuptools_scm
	py3-sphinx
	py3-sphinx-autorun
	py3-sphinx-click
	py3-sphinx_rtd_theme
	py3-wheel
	"
checkdepends="
	py3-freezegun
	py3-hypothesis
	py3-pytest
	py3-pytest-cov
	"
subpackages="$pkgname-doc $pkgname-bash-completion $pkgname-zsh-completion:zshcomp:noarch $pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/${_pyname:0:1}/$_pyname/$_pyname-$pkgver.tar.gz"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
	make -C docs man PYTHONPATH="$PWD"
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	TZ=UTC .testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
	install -Dm644 docs/build/man/todo.1 "$pkgdir"/usr/share/man/man1/todo.1
	install -Dm644 contrib/completion/bash/_todo \
		"$pkgdir"/usr/share/bash-completion/completions/_todo
	install -Dm644 contrib/completion/zsh/_todo \
		"$pkgdir"/usr/share/zsh/site-functions/_todo
}

zshcomp() {
	default_zshcomp
	depends="$depends jq"
}

sha512sums="
9141f14cb1e37a92aedbccbfb0b8be0359e50d922bbbfc939d9696b7943ec152e4645addb9730a4881a0491e58df848faefb8b1870519fbad7be9d438b06c8db  todoman-4.3.1.tar.gz
"
