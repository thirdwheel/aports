# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kwindowsystem
pkgver=5.109.0
pkgrel=0
pkgdesc="Access to the windowing system"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="MIT AND (LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="qt5-qtx11extras-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	libxrender-dev
	qt5-qttools-dev
	samurai
	xcb-util-keysyms-dev
	xcb-util-wm-dev
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/kwindowsystem.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwindowsystem-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kwindowsystem.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# kwindowsystem-kwindowinfox11test hangs
	# kwindowsystem-kwindowsystemx11test, kwindowsystem-kwindowsystem_threadtest and kwindowsystem-netrootinfotestwm are broken
	# kwindowsystem-netwininfotestwm is broken on s390x
	local skipped_tests="kwindowsystem-("
	local tests="
		kwindowinfox11test
		kwindowsystemx11test
		kwindowsystem_threadtest
		netrootinfotestwm
		"
	case "$CARCH" in
		s390x) tests="$tests
			netwininfotestwm
			"
	esac
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)"
	xvfb-run ctest --test-dir build --output-on-failure -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6ae3ccd9a01c592bbc4decc9fbe481a1fbee0168b0ed0774658ea50135199becb628bcdeea440847bae6c600ed01d160f1a1513d7849baf41d461f1e32b8e970  kwindowsystem-5.109.0.tar.xz
"
